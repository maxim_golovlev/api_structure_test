//
//  APIClient.swift
//  APIStructureTest
//
//  Created by Максим Головлев on 21/01/2020.
//  Copyright © 2020 Максим Головлев. All rights reserved.
//

import Foundation
import UIKit

enum APIError: Error {
    
    case badURLRequest
    case badStatusCode(code: Int)
    case customError(message: String)
    
    var localizedDescription: String {
        switch self {
        case .badURLRequest:
            return "Parameters of provided url request is invalid"
        case .badStatusCode(let code):
            return HTTPURLResponse.localizedString(forStatusCode: code)
        case .customError(let message):
            return message
        }
    }
}



