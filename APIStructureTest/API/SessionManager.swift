//
//  SessionManager.swift
//  APIStructureTest
//
//  Created by Максим Головлев on 31/01/2020.
//  Copyright © 2020 Максим Головлев. All rights reserved.
//

import Foundation

typealias HTTPHeaders = [String: String]

protocol SessionManagable: class {
    static var `default`: SessionManagable { get }
    static var acceptEncoding: String { get }
    static var acceptLanguage: String { get }
    static var userAgent: String { get }
    static var defaultHTTPHeaders: HTTPHeaders { get }
    
    var session: URLSession { get set }
    
    init(configuration: URLSessionConfiguration, delegate: URLSessionDelegate?)
}

extension SessionManagable {
    
    static var defaultHTTPHeaders: HTTPHeaders {
        return [
            "Accept-Encoding": acceptEncoding,
            "Accept-Language": acceptLanguage,
            "User-Agent": userAgent
        ]
    }
    
    static var acceptEncoding: String {
        return "gzip;q=1.0, compress;q=0.5"
    }
    
    static var acceptLanguage: String {
        return Locale.preferredLanguages.prefix(6).enumerated().map { index, languageCode in
            let quality = 1.0 - (Double(index) * 0.1)
            return "\(languageCode);q=\(quality)"
        }.joined(separator: ", ")
    }
    
    static var userAgent: String {
        if let info = Bundle.main.infoDictionary {
            let executable = info[kCFBundleExecutableKey as String] as? String ?? "Unknown"
            let bundle = info[kCFBundleIdentifierKey as String] as? String ?? "Unknown"
            let appVersion = info["CFBundleShortVersionString"] as? String ?? "Unknown"
            let appBuild = info[kCFBundleVersionKey as String] as? String ?? "Unknown"

            let osNameVersion: String = {
                let version = ProcessInfo.processInfo.operatingSystemVersion
                let versionString = "\(version.majorVersion).\(version.minorVersion).\(version.patchVersion)"

                let osName: String = {
                    #if os(iOS)
                        return "iOS"
                    #elseif os(watchOS)
                        return "watchOS"
                    #elseif os(tvOS)
                        return "tvOS"
                    #elseif os(macOS)
                        return "OS X"
                    #elseif os(Linux)
                        return "Linux"
                    #else
                        return "Unknown"
                    #endif
                }()

                return "\(osName) \(versionString)"
            }()

            return "\(executable)/\(appVersion) (\(bundle); build:\(appBuild); \(osNameVersion))"
        }
        
        return ""
    }
}

class Retrier {
    private var count: Int
    private var delay: Double = 0
    
    init(count: Int, delay: Double = 0) {
        self.count = count
        self.delay = delay
    }
    
    class var `default`: Retrier {
        return Retrier(count: 3, delay: 3)
    }
    
    func retry(completion: @escaping () -> Void) throws {
        guard canTry else {
            throw APIError.customError(message: "Retry attempts is over")
        }
        count = count - 1
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: completion)
    }
    
    var canTry: Bool {
        return count > 0
    }
}


extension SessionManagable {
    
    func perform(request: URLRequest?, oldPromise: Promise<(Data, URLResponse)>? = nil, retrier: Retrier = Retrier.default) -> Future<(Data, URLResponse)> {
        
        let promise = oldPromise ?? Promise<(Data, URLResponse)>()
        
        guard let request = request else {
            promise.reject(with: APIError.customError(message: "URLRequest is invalid"))
            return promise
        }
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                do {
                    try retrier.retry {
                        print("attempt to retry")
                        _ = self.perform(request: request, oldPromise: promise, retrier: retrier)
                    }
                } catch _ {
                    promise.reject(with: error)
                }
                
            } else {
                promise.resolve(with: (data ?? Data(), response ?? URLResponse()))
            }
        }
        
        task.resume()
        
        promise.task = task
        promise.request = request
        
        return promise
    }
}

class SessionManager: SessionManagable {
    
    static var `default`: SessionManagable {
        return SessionManager(configuration: URLSessionConfiguration.default, delegate: nil)
    }
    
    var session: URLSession
    
    required init(configuration: URLSessionConfiguration, delegate: URLSessionDelegate?) {
        self.session = URLSession.init(configuration: configuration)
    }
}


