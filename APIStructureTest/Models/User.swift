//
//  User.swift
//  APIStructureTest
//
//  Created by Максим Головлев on 07/02/2020.
//  Copyright © 2020 Максим Головлев. All rights reserved.
//

import Foundation

struct User: Archivable {
    let id: Int
    let email: String
    let firstName: String
    let lastName: String
    let avatar: String
}
