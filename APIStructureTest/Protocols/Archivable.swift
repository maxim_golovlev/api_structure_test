//
//  Savable.swift
//  APIStructureTest
//
//  Created by Максим Головлев on 07/02/2020.
//  Copyright © 2020 Максим Головлев. All rights reserved.
//

import Foundation


enum ArchiveError: Error {
    case archiveError
}

protocol Archivable: Codable {
    
    func archive() throws
    func archiveAdd() throws
    static func unarchive() throws -> [Self]
    static func delete() throws
}

extension Array: Archivable where Element: Archivable {
    
    func archive() throws {
        let data = try JSONEncoder().encode(self)
        try data.write(to: Element.self.fileUrl)
    }
}

extension Archivable {
    
    static var fileUrl: URL {
        return FileManager().urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("\(Self.self)")
    }
    
    func archive() throws {
        let data = try JSONEncoder().encode([self])
        try data.write(to: Self.fileUrl)
    }
    
    func archiveAdd() throws {
        var oldItems = try Self.self.unarchive()
        oldItems.append(self)
        try oldItems.archive()
    }
    
    static func unarchive() throws -> [Self] {
        let data = try Data(contentsOf: fileUrl)
        let objects = try JSONDecoder().decode([Self].self, from: data)
        return objects
    }
    
    static func delete() throws {
        try FileManager().removeItem(atPath: Self.fileUrl.path)
    }
    
}
