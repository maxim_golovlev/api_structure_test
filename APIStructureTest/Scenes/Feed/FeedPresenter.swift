//
//  FeedPresenter.swift
//  APIStructureTest
//
//  Created by Максим Головлев on 06/02/2020.
//  Copyright © 2020 Максим Головлев. All rights reserved.
//

import Foundation

protocol FeedDisplayProtocol: class {
    func displayUsers(users: [User])
    func failedToFetchUsers(with error: Error)
}

class FeedPresenter {
    
    init(vc: FeedDisplayProtocol?) {
        self.viewController = vc
    }
    
    weak var viewController: FeedDisplayProtocol?
    
    func fetchUsers(page: Int) -> URLSessionDataTask? {
        
        return UserService().fetchUsers(page: 0) { [weak self] (result) in
            
            switch result {
            case .success(let items):
                self?.viewController?.displayUsers(users: items)
            case .failure(let error):
                self?.viewController?.failedToFetchUsers(with: error)
            }
            
        }
        
    }
    
    func fetchSavedUsers() {
        do {
            let users = try User.unarchive()
            viewController?.displayUsers(users: users)
        } catch {
            viewController?.failedToFetchUsers(with: error)
        }
    }
    
}
