//
//  ViewController.swift
//  APIStructureTest
//
//  Created by Максим Головлев on 21/01/2020.
//  Copyright © 2020 Максим Головлев. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    lazy var presenter = FeedPresenter(vc: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let task = presenter.fetchUsers(page: 0)
      //  task?.cancel()
        
      //  presenter.fetchSavedUsers()
    }
    
}

extension ViewController: FeedDisplayProtocol {
    func displayUsers(users: [User]) {
        print(users)
    }
    
    func failedToFetchUsers(with error: Error) {
        print(error)
    }
}
