//
//  UserService.swift
//  APIStructureTest
//
//  Created by Максим Головлев on 06/02/2020.
//  Copyright © 2020 Максим Головлев. All rights reserved.
//

import Foundation

class UserService: APIService {
    
    enum router: APIRouter {
        
        case fetchUsers(page: Int)
        
        var action: String {
            return "/api/users"
        }
        
        var parameters: [String : String] {
            switch self {
            case .fetchUsers(let page):
                return ["page": "\(page)"]
            }
        }
        
        var dataKey: String? {
            return "data"
        }
        
        var method: HTTPMethod {
            return .get
        }
    }
    
    func fetchUsers(page: Int, completion: @escaping (Result<[User], Error>) -> Void) -> URLSessionDataTask? {
        
        let r = router.fetchUsers(page: page)
        
        let task = SessionManager.default
            .perform(request: r.asURLRequest())
            .validate(using: acceptableStatusCodes)
            .decoded(using: jsonDecoder, keypath: r.dataKey)
            .saved()
            .observe(using: completion)
        
        return task
    }
    
}
